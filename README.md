# Spring Hazelcast Cache

Run this project by this command : `mvn clean spring-boot:run`

### Screenshot

Get All User from DB

![Get All User](img/load1.png "Get All User from DB")

Get All User from Hazelcast

![Get All User from Hazelcast](img/load-cache.png "Get All User from Hazelcast")

Find User By Id from DB

![Find User By Id from DB](img/find1.png "Find User By Id from DB")

Find User By Id from Hazelcast

![Find User By Id from Hazelcast](img/find2.png "Find User By Id from Hazelcast")




