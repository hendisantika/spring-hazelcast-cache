package com.hendisantika.springhazelcastcache.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-hazelcast-cache
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-03
 * Time: 07:19
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Data
@AllArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = -1818061710613422175L;

    @Id
    private int id;
    private String name;
    private String address;

    public User() {
        super();
    }

}