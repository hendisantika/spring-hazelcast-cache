package com.hendisantika.springhazelcastcache.controller;

import com.hendisantika.springhazelcastcache.model.User;
import com.hendisantika.springhazelcastcache.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-hazelcast-cache
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-03
 * Time: 07:24
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/hazelcast-cache")
public class UserController {
    @Autowired
    private UserService service;

    @GetMapping("/getAllUsers")
    public List<User> getUsers() {
        return service.getUsers();
    }

    @GetMapping("/findById/{id}")
    public User findUserById(@PathVariable int id) {
        return service.getUserById(id);
    }

    @PutMapping("/update/{id}")
    public String updateUser(@PathVariable int id, @RequestParam("address") String address) {
        return service.updateUser(id, address);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable int id) {
        return service.deleteUser(id);
    }

}
